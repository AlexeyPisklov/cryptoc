<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ClearHighchartsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'highcharts:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear the chart on the currency page';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $connection = \DB::connection('mysql2');

        $tables = $connection->select('SELECT TABLE_NAME AS name 
            FROM INFORMATION_SCHEMA.TABLES 
            WHERE TABLE_SCHEMA = :database',
            [
                'database' => env('DB_DATABASE2')
            ]
        );

        foreach($tables as $table)
        {
            $tableName = $table->name;

            $id = collect($connection->select("SELECT t.id, t.created_at FROM `{$tableName}` AS t
                                 LEFT JOIN `{$tableName}` AS t2 ON t2.id = t.id+1 AND (unix_timestamp(t.created_at) - unix_timestamp(t2.created_at)) >= -1850
                                 WHERE unix_timestamp(t2.created_at) IS NULL
                                 AND t.id != (SELECT max(id) FROM `{$tableName}`)
                                 ORDER BY t.id DESC
                                 LIMIT 1"))->first();

            if($id === null)
            {
                $this->info("Table {$tableName} is normal");
                continue;
            }
            else
            {
                $this->warn("Fixed: {$tableName} = {$id->id} :: {$id->created_at} ");
                $connection->delete("DELETE FROM `{$tableName}` WHERE id <= :id", ['id' => $id->id]);
            }
        }

    }
}
