<?php

namespace App\Observers;

use App\GlobalData;

class BeforeSavingGlobalDataObserver
{
    public function saving(GlobalData $record)
    {
        $record->percent_change_7d  = is_null($record->percent_change_7d) ? 0 : $record->percent_change_7d;
        $record->percent_change_24h = is_null($record->percent_change_24h) ? 0 : $record->percent_change_24h;
        $record->percent_change_1h  = is_null($record->percent_change_1h) ? 0 : $record->percent_change_1h;
        $record->market_cap_usd     = is_null($record->market_cap_usd) ? 0 : $record->market_cap_usd;
        $record->price_usd          = is_null($record->price_usd) ? 0 : $record->price_usd;
        $record->price_btc          = is_null($record->price_btc) ? 0 : $record->price_btc;
        $record->volume_usd_24h     = is_null($record->volume_usd_24h) ? 0 : $record->volume_usd_24h;
        $record->available_supply   = is_null($record->available_supply) ? 0 : $record->available_supply;
        $record->total_supply       = is_null($record->total_supply) ? 0 : $record->total_supply;
        $record->last_updated       = is_null($record->last_updated) ? 0 : $record->last_updated;
    }

    public function updating(GlobalData $record)
    {
        $record->percent_change_7d = is_null($record->percent_change_7d) ? 0 : $record->percent_change_7d;
        $record->percent_change_24h = is_null($record->percent_change_24h) ? 0 : $record->percent_change_24h;
        $record->percent_change_1h = is_null($record->percent_change_1h) ? 0 : $record->percent_change_1h;
        $record->market_cap_usd = is_null($record->market_cap_usd) ? 0 : $record->market_cap_usd;
        $record->price_usd = is_null($record->price_usd) ? 0 : $record->price_usd;
        $record->price_btc = is_null($record->price_btc) ? 0 : $record->price_btc;
        $record->volume_usd_24h = is_null($record->volume_usd_24h) ? 0 : $record->volume_usd_24h;
        $record->available_supply = is_null($record->available_supply) ? 0 : $record->available_supply;
        $record->total_supply = is_null($record->total_supply) ? 0 : $record->total_supply;
        $record->last_updated = is_null($record->last_updated) ? 0 : $record->last_updated;
    }
}
