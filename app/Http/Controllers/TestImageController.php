<?php
namespace App\Http\Controllers;

use App\GlobalData;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use GuzzleHttp;
use App\currencyHistory;

class TestImageController extends Controller
{
    protected $outParams = []; //Убрать outParams!

    public function index()
    {
        try {
            $tables = array_map('reset', \DB::connection('mysql2')->select('SHOW TABLES'));

            $template = 'SELECT * FROM `%s` WHERE ';
            $template .= '`created_at` >= curdate() - INTERVAL DAYOFWEEK(curdate())+7 DAY AND ';
            $template .= '`created_at` <= curdate() ORDER BY created_at ASC';

            foreach ($tables as $table)
            {
                $sql = sprintf($template, $table);
                $cryptoHistory = \DB::connection('mysql2')->select($sql);

                $data = array();
                foreach ($cryptoHistory as $history)
                {
                    $data[] = doubleval($history->price_usd);
                }

                $pairs = $this->saveToJson($data, $table);
            }

        }
        catch(\Exception $e)
        {
            var_dump($e->getMessage());
        }


        $resp = $this->createGraph($pairs);

        self::updateCssImg();
        $this->updateIconsCrypto();
        $this->currencyPerDay();

        return $resp;
    }

    protected function createGraph()
    {
        $chunks = array_chunk($this->outParams, 100);
        foreach ($chunks as $chunk)
        {
            $runServer = 'highcharts-export-server --batch "'. implode(';', $chunk ) .'"';
            try
            {
                $res = shell_exec($runServer);
            }
            catch(\Exception $e)
            {
                return 'ERROR: ' . $e->getMessage();
            }
        }
        return $res;
    }

    protected function saveToJson($data, $fileName)
    {
        $standartResource = public_path() . '/resources.json';
        $resourceJson = json_decode(file_get_contents($standartResource), true);

        $resourceJson['series'] = [
            ["data" => $data, "type" => "line"]
        ];

        $newResource = 'resource/resource-'. $fileName .'.json';

        Storage::disk('local')->put($newResource, json_encode($resourceJson));

        $this->outParams[] = storage_path('app').'/'.$newResource . '=' . $_SERVER['DOCUMENT_ROOT'] . '/img/crypto/' . $fileName . '.png';
    }

    static function updateCssImg()
    {
        $returned_content = self::getData('https://coinmarketcap.com/static/sprites/all_views_all_0.css');
        if ($returned_content != null) {
            Storage::disk('public')->put('css/cryptoicons.css', $returned_content);
        }
        $returned_content = self::getData('https://coinmarketcap.com/static/sprites/all_views_all_0.png');
        if ($returned_content != null) {
            Storage::disk('public')->put('css/all_views_all_0.png', $returned_content);
        }
    }

    static function getData($url)
    {
        $client = new GuzzleHttp\Client();
        try {
            $res = $client->request('GET', $url, []);
            if ($res->getStatusCode() == 200) {
                $data = $res->getBody();
                return $data->getContents();
            }
        } catch(\Exception $e) {
            return null;
        }
        return null;

    }

    public function updateIconsCrypto()
    {
        $allCryptoAPI = GlobalData::all();
        foreach ($allCryptoAPI as $item)
        {
            $name = $item->name;
            $id = $item->id;

            try
            {
                $returned_content = self::getData('https://files.coinmarketcap.com/static/img/coins/32x32/' . "$id" . '.png');
                if ($returned_content != null)
                {
                    Storage::disk('public')->put('img/icons/' . $id . '.png', $returned_content);
                }
                else
                {
                    $returned_content = self::getData('https://files.coinmarketcap.com/static/img/coins/32x32/' . "$name" . '.png');
                    if ($returned_content != null)
                    {
                        Storage::disk('public')->put('img/icons/' . $name . '.png', $returned_content);
                    }
                }
            }
            catch (\Illuminate\Database\QueryException $e)
            {

            }
        }
    }

    public function currencyPerDay()
    {
        $data = \App\ExchangeRatesCap\Base::getExchangeRates();
        foreach ($data['quotes'] as $key => $datum) {
            $name = str_replace('USD', '', $key);
            currencyHistory::updateOrCreate(
                [
                    'name' => "{$name}"
                ],
                [
                    'price_old' => "{$datum}",
                    'crypto' => 0
                ]
            );
        }
        $data = \App\CoinMarketCap\Base::getGlobalData();
        foreach ($data as $key => $item) {
            currencyHistory::updateOrCreate(
                [
                    'name' => "{$item['id']}"
                ],
                [
                    'price_old' => "{$item['price_usd']}",
                    'crypto' => 1
                ]
            );
        }
        return $data;
    }
}
