<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Artisan;

class CacheController extends Controller
{
    public function clear()
    {
        try {
            Artisan::call('cache:clear');
        } catch (\Exception $e) {
            var_dump($e->getMessage());
        }
    }
}