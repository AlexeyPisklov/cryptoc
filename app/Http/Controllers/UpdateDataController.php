<?php

namespace App\Http\Controllers;

use App\Observers\BeforeSavingGlobalDataObserver;
use App\ExchangeRate;
use App\CoinMarketCap;
use App\GlobalData;
use App\ExchangeRatesCap;
use App\Search;
use App\TotalMarketCap;
use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class UpdateDataController extends Controller
{
    public function __construct()
    {
        GlobalData::observe(new BeforeSavingGlobalDataObserver);
    }

    /**
     * Store all data from the coinmarketcap
     *
     * @param  int $id
     * @return Response
     */
    public function storeAllFrom()
    {
        $btc_currencies = CoinMarketCap\Base::getGlobalData();

        $totalMarketCap = 0;

        foreach ($btc_currencies as $btc_currency)
        {
            $totalMarketCap += $btc_currency['market_cap_usd'];

            $globalDataCurency = GlobalData::findOrNew($btc_currency['id']);
            $globalDataCurency->price_usdOld = $globalDataCurency->price_usd;
            $globalDataCurency->fill($btc_currency);
            $globalDataCurency->save();

            $tableName = $btc_currency['id'];
            if (($schema = Schema::connection('mysql2'))->hasTable($tableName) === false)
            {
                try
                {
                    $schema->create(quotemeta($tableName), function ($table) {
                        $table->increments('id');
                        $table->double('price_usd');
                        $table->dateTime('created_at');
                    });
                }
                catch (\Illuminate\Database\QueryException $e)
                {
                    dd($globalDataCurency);
                }
                catch (PDOException $e)
                {
                    dd($e);
                }
            }

            DB::connection('mysql2')->table(quotemeta($tableName))->insert(['price_usd' => $globalDataCurency->price_usd, 'created_at' => Carbon::now()]);
        }

        TotalMarketCap::updateOrCreate(['id' => 1 ], ['price' => $totalMarketCap]);
        $this->updateSearchTable();
        return 'Updated successfully';
    }
    /**
     * Store all data from the ExchangeRates
     *
     * @return Response
     */
    public function storeExchangeRates()
    {
        $data = ExchangeRatesCap\Base::getExchangeRates();
        foreach ($data['quotes'] as $key => $item) {
            if ($key != "USDBTC") {

                $oldItem = ExchangeRate::where('name_quotes', 'like', $key)->get()->first();
                if ($oldItem === null) {
                    $exchange = new ExchangeRate;

                    $exchange->name_quotes = $key;
                    $exchange->value_quotes = $item;
                    $exchange->value_quotesOld = $item;
                    $exchange->source = $data['source'];
                    $exchange->timestamp = \Carbon\Carbon::now();
                    $exchange->save();

                    //need to creqate new one;
                } else {
                    //need to get old value_quotes an put to value_quotesOld
                    //put new value_quotes
                    $oldItem->value_quotesOld = $oldItem->value_quotes;
                    $oldItem->value_quotes = $item;
                    $oldItem->source = $data['source'];
                    $oldItem->timestamp = \Carbon\Carbon::now();
                    $oldItem->save();
                }
            }
        }
        $this->updateSearchTable();
        return 'Updated successfully';
    }

    private function updateSearchTable()
    {
        $listElements = Search\Base::generateListElements();
        foreach ($listElements as $listElement) {
            Search::updateOrCreate(
                [
                    'id' => "{$listElement['id']}"
                ],[
                    "price_usd" => $listElement['price_usd'],
                    "type" => $listElement['type'],
                    "profile_long" => $listElement['profile_long'],
                ]
            );
        }
    }
}
