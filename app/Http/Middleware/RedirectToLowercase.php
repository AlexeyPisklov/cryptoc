<?php
/**
 * Created by PhpStorm.
 * User: mmpc1
 * Date: 01.12.17
 * Time: 13:39
 */

namespace App\Http\Middleware;


class RedirectToLowercase
{
    public function handle($request, \Closure $next) {
        $path = $request->path();
        $pathLowercase = strtolower($path); // convert to lowercase

        if ($path !== $pathLowercase) {
            // redirect if lowercased path differs from original path
            return redirect($pathLowercase);
        }

        return $next($request);
    }
}