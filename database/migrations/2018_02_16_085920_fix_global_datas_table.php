<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixGlobalDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('global_datas', function (Blueprint $table) {
            $table->float('rank',16, 2)->default(0)->nullable(false)->change();
            $table->float('price_usd',16, 2)->default(0)->nullable(false)->change();
            $table->float('price_btc',16, 2)->default(0)->nullable(false)->change();
            $table->float('volume_usd_24h',16, 2)->default(0)->nullable(false)->change();
            $table->float('market_cap_usd',16, 2)->default(0)->nullable(false)->change();
            $table->float('available_supply',16, 2)->default(0)->nullable(false)->change();
            $table->float('total_supply',16, 2)->default(0)->nullable(false)->change();
            $table->float('percent_change_1h',16, 2)->default(0)->nullable(false)->change();
            $table->float('percent_change_24h',16, 2)->default(0)->nullable(false)->change();
            $table->float('percent_change_7d',16, 2)->default(0)->nullable(false)->change();
            $table->integer('last_updated')->default(0)->change();
            $table->float('price_usdOld',16, 2)->default(0)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('global_datas', function (Blueprint $table) {
            $table->float('rank',16, 2)->default(null)->nullable(true)->change();
            $table->float('price_usd',16, 2)->default(null)->nullable(true)->change();
            $table->float('price_btc',16, 2)->default(null)->nullable(true)->change();
            $table->float('volume_usd_24h',16, 2)->default(null)->nullable(true)->change();
            $table->float('market_cap_usd',16, 2)->default(null)->nullable(true)->change();
            $table->float('available_supply',16, 2)->default(null)->nullable(true)->change();
            $table->float('total_supply',16, 2)->default(null)->nullable(true)->change();
            $table->float('percent_change_1h',16, 2)->default(null)->nullable(true)->change();
            $table->float('percent_change_24h',16, 2)->default(null)->nullable(true)->change();
            $table->float('percent_change_7d',16, 2)->default(null)->nullable(true)->change();
            $table->integer('last_updated')->default(null)->nullable(true)->change();
            $table->float('price_usdOld',16, 2)->default(null)->nullable(true)->change();
        });
    }
}
